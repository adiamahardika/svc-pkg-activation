package trilogi.myg.svc.pkg.activation.model;

public class ChargeList {
	private String price;
    
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
