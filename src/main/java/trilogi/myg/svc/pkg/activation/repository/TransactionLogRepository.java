package trilogi.myg.svc.pkg.activation.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import trilogi.myg.svc.pkg.activation.entity.TransactionLog;

@Repository
public interface TransactionLogRepository extends CrudRepository<TransactionLog, Long> {
}
