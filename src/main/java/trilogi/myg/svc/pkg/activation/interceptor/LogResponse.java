package trilogi.myg.svc.pkg.activation.interceptor;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.springframework.http.HttpHeaders;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LogResponse {
    private String responseTime;
    private String statusCode;
    private String statusText;
    private HttpHeaders header;
    private String responseBody;

    public LogResponse(String responseTime, String statusCode, String statusText, HttpHeaders header, String responseBody) {
        this.responseTime = responseTime;
        this.statusCode = statusCode;
        this.statusText = statusText;
        this.header = header;
        this.responseBody = responseBody;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public HttpHeaders getHeader() {
        return header;
    }

    public void setHeader(HttpHeaders header) {
        this.header = header;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }
}
