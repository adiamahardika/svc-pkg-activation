package trilogi.myg.svc.pkg.activation.constant;

public class GeneralConstants {
	public static final String RC_SUCCESS = "00";
    public static final String RC_SUCCESS_PRO = "0000";
    public static final String RC_SUCCESS_MESSAGE = "success";
    public static final String RC_EXCEPTION = "01";
    public static final String RC_WARNING = "02";
    
    public static final String COMPLETED = "Completed";
    public static final String CANCELED = "Canceled";
    public static final String PAID = "Paid";
    public static final String REFUNDED = "Refunded";
    
    public static final String AKTIF = "A";
    
    public static final String NO_DATA = "-";
    
    public static final String POSTPAID_TYPE = "Hybrid Subscriber";
    public static final String PREPAID_TYPE = "Online or prepaid subscriber";
    public static final String CORPORATE_ACCOUNT_TYPE = "Corporate";
    
    public static final String IS_AKTIF = "Y";
}
