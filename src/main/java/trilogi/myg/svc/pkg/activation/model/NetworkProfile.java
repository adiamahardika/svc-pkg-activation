package trilogi.myg.svc.pkg.activation.model;

public class NetworkProfile {
	private String subscriberType;
	private String scpId;
	
	public String getSubscriberType() {
		return subscriberType;
	}
	
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}
	
	public String getScpId() {
		return scpId;
	}
	
	public void setScpId(String scpId) {
		this.scpId = scpId;
	}
}
