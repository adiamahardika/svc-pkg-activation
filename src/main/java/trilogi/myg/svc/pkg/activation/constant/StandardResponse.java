package trilogi.myg.svc.pkg.activation.constant;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import trilogi.myg.svc.pkg.activation.model.Transaction;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StandardResponse implements Serializable{
	
	private static final long serialVersionUID = 5029782169535879696L;
	
	private Transaction transaction;
	
	public StandardResponse() {
		this.transaction = new Transaction();
	}
	
	public Transaction getTransaction() {
		return transaction;
	}
	
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

}
