package trilogi.myg.svc.pkg.activation.general;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import trilogi.myg.svc.pkg.activation.constant.GeneralConstants;
import trilogi.myg.svc.pkg.activation.constant.StandardResponse;
import trilogi.myg.svc.pkg.activation.entity.Terminal;
import trilogi.myg.svc.pkg.activation.logging.service.LoggingService;
import trilogi.myg.svc.pkg.activation.model.Transaction;
import trilogi.myg.svc.pkg.activation.repository.TerminalRepository;

@Service("encryptionValidationService")
public class EncryptionValidationService {

    @Autowired
    private TerminalRepository terminalRepo;

    @Autowired
    private LoggingService loggingService;

    public Terminal validationSignatureTerminal(String terminalId, String secretKey, String signatureKey) {
        Terminal terminal = null;
        try {
            terminal = terminalRepo.findByTerminalIdAndStatus(terminalId, GeneralConstants.AKTIF);
            if(terminal !=  null) {
                String signature = terminalId + secretKey + terminal.getConfig().getToken();
                String signatureHash = DigestUtils.md5DigestAsHex(signature.getBytes());
                if(!signatureHash.equalsIgnoreCase(signatureKey)) {
                    terminal = null;
                }
            } 
        } catch (Exception error) {
            error.printStackTrace();
        }
        return terminal;
    }

    public boolean validationSignature(String terminalId, String secretKey, String signatureKey) {
        boolean valid = false;
        try {
            Terminal terminal = terminalRepo.findByTerminalIdAndStatus(terminalId, GeneralConstants.AKTIF);
            if(terminal !=  null) {
                String signature = terminalId + secretKey + terminal.getConfig().getToken();
                String signatureHash = DigestUtils.md5DigestAsHex(signature.getBytes());
                if(signatureHash.equalsIgnoreCase(signatureKey)) {
                    valid = true;
                }
            } else {
                return valid;
            }
        } catch (Exception error) {
            if(error instanceof DataAccessException) {
                return valid;
            }
            return valid;
        }
        return valid;
    }
    
    public boolean validationSignatureLogin(String terminalId, String secretKey, String signatureKey, String token) {
        boolean valid = false;
        try {
            Terminal terminal = terminalRepo.findByTerminalIdAndStatus(terminalId, GeneralConstants.AKTIF);
            if(terminal !=  null) {
                long timestamp = loggingService.getUniqueCurrentTime()/1000L;
                int a = 0;

                while(a <= 180 && !valid) {
                    String signature = terminalId + secretKey + terminal.getConfig().getToken() + (timestamp - a);
                    String signatureHash = DigestUtils.md5DigestAsHex(signature.getBytes());
                    if(signatureHash.equalsIgnoreCase(signatureKey)) {
                        valid = true;
                    }
                    a++;
                }
                if(valid) {

                }
            } else {
                return valid;
            }
        } catch (Exception error) {
            if(error instanceof DataAccessException) {
                return valid;
            }
            return valid;
        }
        return valid;
    }

    public ResponseEntity<Object> handleInvalidSignature() {
        StandardResponse esb = new StandardResponse();
        Transaction trx = new Transaction();
        trx.setStatusCode(GeneralConstants.RC_EXCEPTION);
        trx.setErrorCode("403");
        trx.setStatusDesc("Invalid Authorization");
        esb.setTransaction(trx);
        return new ResponseEntity<>(esb, HttpStatus.FORBIDDEN);
    }

}

