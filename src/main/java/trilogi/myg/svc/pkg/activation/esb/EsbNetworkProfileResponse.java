package trilogi.myg.svc.pkg.activation.esb;

import trilogi.myg.svc.pkg.activation.model.NetworkProfile;

public class EsbNetworkProfileResponse extends EsbResponse {
	private NetworkProfile networkProfile;
	
	public NetworkProfile getNetworkProfile() {
		return networkProfile;
	}
	
	public void setNetworkProfile(NetworkProfile networkProfile) {
		this.networkProfile = networkProfile;
	}
	
}
