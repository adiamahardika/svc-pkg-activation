package trilogi.myg.svc.pkg.activation.model;

import java.util.List;

public class OffersList {
	private String mainClass;
	private String productsOfferId;
	private String productOfferName;
	private String offerSubType;
	private String activePeriod;
	private List<Bonus> bonus;
	
	
	public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }
    
	public String getProductsOfferId() {
        return productsOfferId;
    }

    public void setProductsOfferId(String productsOfferId) {
        this.productsOfferId = productsOfferId;
    }
    
    public String getProductOfferName() {
        return productOfferName;
    }

    public void setProductOfferName(String productOfferName) {
        this.productOfferName = productOfferName;
    }
    
    public String getOfferSubType() {
        return offerSubType;
    }

    public void setOfferSubType(String offerSubType) {
        this.offerSubType = offerSubType;
    }
    
    public String getActivePeriod() {
        return activePeriod;
    }

    public void setActivePeriod(String activePeriod) {
        this.activePeriod = activePeriod;
    }
    
    public List<Bonus> getBonus() {
        return bonus;
    }

    public void setBonus(List<Bonus> bonus) {
        this.bonus = bonus;
    }
}
