package trilogi.myg.svc.pkg.activation.bisnis.paket;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import trilogi.myg.svc.pkg.activation.constant.StandardResponse;
import trilogi.myg.svc.pkg.activation.model.Account;
import trilogi.myg.svc.pkg.activation.model.Bill;
import trilogi.myg.svc.pkg.activation.model.NetworkProfile;
import trilogi.myg.svc.pkg.activation.model.Offer;
import trilogi.myg.svc.pkg.activation.model.Services;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetPaketResponse extends StandardResponse {
	
	private static final long serialVersionUID = 5029782169535879696L;
	
	private Services service;
	private NetworkProfile networkProfile;
	private Account account;
	private Bill bill;
	private String confirmationMessage;
	private List<Offer> offer;
	
	public Services getService() {
		return service;
	}
	
	public void setService(Services service) {
		this.service = service;
	}
	
	public NetworkProfile getNetworkProfile() {
		return networkProfile;
	}
	
	public void setNetworkProfile(NetworkProfile networkProfile) {
		this.networkProfile = networkProfile;
	}
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Bill getBill() {
		return bill;
	}
	
	public void setBill(Bill bill) {
		this.bill = bill;
	}
	
	public String getConfirmationMessage() {
		return confirmationMessage;
	}
	
	public void setConfirmationMessage(String confirmationMessage) {
		this.confirmationMessage = confirmationMessage;
	}
	
	public List<Offer> getOffer() {
		return offer;
	}
	
	public void setOffer(List<Offer> offer) {
		this.offer = offer;
	}

}
