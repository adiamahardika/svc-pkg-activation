package trilogi.myg.svc.pkg.activation.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "ms_voucher_pulsa")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class VoucherPulsa {
    @GenericGenerator(
        name = "msVoucherPulsaIdSeqGenerator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "msVoucherPulsaIdSeq"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
        }
    )

    @Id
    @GeneratedValue(generator = "msVoucherPulsaIdSeqGenerator")
    @JsonIgnore
    private Long id;

    @Column(name = "nominal")
    private Double nominal;

    @Column(name = "label_nominal")
    private String labelNominal;

    @Column(name = "icon")
    private String icon;

    @JsonIgnore
    @Column(name = "is_aktif")
    private String isAktif;

    @JsonIgnore
    @Column(name = "created_by")
    private String createdBy;

    @JsonIgnore
    @Column(name = "created_date")
    private Timestamp createdDate;

    @JsonIgnore
    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnore
    @Column(name = "updated_date")
    private Timestamp updatedDate;

    public Long getId() {
        return id;
    }

    public Double getNominal() {
        return nominal;
    }

    public String getLabelNominal() {
        return labelNominal;
    }

    public String getIcon() {
        return icon;
    }

    public String getIsAktif() {
        return isAktif;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }
}
