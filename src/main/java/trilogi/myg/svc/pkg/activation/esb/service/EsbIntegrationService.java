package trilogi.myg.svc.pkg.activation.esb.service;

import com.google.gson.Gson;

import trilogi.myg.svc.pkg.activation.entity.LgServiceActivities;
import trilogi.myg.svc.pkg.activation.esb.EsbCheckPrepaidBalanceResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbCheckPostpaidBalanceRequest;
import trilogi.myg.svc.pkg.activation.esb.EsbCheckPostpaidBalanceResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbCheckPrepaidBalanceRequest;
import trilogi.myg.svc.pkg.activation.esb.EsbCustomerProfileRequest;
import trilogi.myg.svc.pkg.activation.esb.EsbCustomerProfileResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbNetworkProfileRequest;
import trilogi.myg.svc.pkg.activation.esb.EsbNetworkProfileResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbProductOfferRequest;
import trilogi.myg.svc.pkg.activation.esb.EsbProductOfferResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbServiceProfileRequest;
import trilogi.myg.svc.pkg.activation.esb.EsbServiceProfileResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbSubmitOrderRequest;
import trilogi.myg.svc.pkg.activation.esb.EsbSubmitOrderResponse;
import trilogi.myg.svc.pkg.activation.logging.service.LoggingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

@Service("esbService")
@RefreshScope
public class EsbIntegrationService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoggingService loggingService;

    @Autowired
    private Gson gson;

    @Value("${url.esb.integration.customer.profile}")
    private String urlEsbCustomerProfile;

    @Value("${url.esb.integration.network.profile}")
    private String urlEsbNetworkProfile;
    
    @Value("${url.esb.integration.check.prepaid.balance}")
    private String urlEsbCheckPrepaidBalance;
    
    @Value("${url.esb.integration.check.postpaid.balance}")
    private String urlEsbCheckPostpaidBalance;
    
    @Value("${url.esb.integration.service.profile}")
    private String urlEsbServiceProfile;
    
    @Value("${url.esb.integration.product.offer}")
    private String urlEsbProductOffer;
    
    @Value("${url.esb.integration.submit.order}")
    private String urlEsbSubmitOrder;

    
    public EsbCustomerProfileResponse getCustomerProfile(String terminalId, String transactionId, String noHp, HttpServletRequest servlet, String moduleName) throws Exception {
        EsbCustomerProfileRequest customerProfile = new EsbCustomerProfileRequest();
        customerProfile.setTerminalId(terminalId);
        customerProfile.setTransactionId(transactionId);
        customerProfile.setNoHp(noHp);
        customerProfile.setModuleName(moduleName);
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        EsbCustomerProfileResponse esbResponse = restTemplate.postForObject(urlEsbCustomerProfile, customerProfile, EsbCustomerProfileResponse.class);
        LgServiceActivities lg;
        lg = new LgServiceActivities(terminalId, transactionId, servlet.getRequestURI(), urlEsbCustomerProfile, gson.toJson(customerProfile), gson.toJson(esbResponse), start, new Timestamp(System.currentTimeMillis()), terminalId, "O");
        loggingService.saveLog(lg);
        return esbResponse;
    }
    
    public EsbNetworkProfileResponse getNetworkProfile(String terminalId, String transactionId, String noHp, HttpServletRequest servlet, String moduleName) throws Exception {
    	EsbNetworkProfileRequest networkProfile = new EsbNetworkProfileRequest();
        networkProfile.setTerminalId(terminalId);
        networkProfile.setTransactionId(transactionId);
        networkProfile.setNoHp(noHp);
        networkProfile.setModuleName(moduleName);
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        EsbNetworkProfileResponse networkProfileResponse = restTemplate.postForObject(urlEsbNetworkProfile, networkProfile, EsbNetworkProfileResponse.class);
        LgServiceActivities lg;
        lg = new LgServiceActivities(terminalId, transactionId, servlet.getRequestURI(), urlEsbCustomerProfile, gson.toJson(networkProfile), gson.toJson(networkProfileResponse), start, new Timestamp(System.currentTimeMillis()), terminalId, "O");
        loggingService.saveLog(lg);
        return networkProfileResponse;
    }
    
    public EsbCheckPrepaidBalanceResponse getCheckPrepaidBalance(String terminalId, String transactionId, String noHp, HttpServletRequest servlet, String moduleName) throws Exception {
    	EsbCheckPrepaidBalanceRequest checkPrepaidBalance = new EsbCheckPrepaidBalanceRequest();
    	checkPrepaidBalance.setTerminalId(terminalId);
    	checkPrepaidBalance.setTransactionId(transactionId);
    	checkPrepaidBalance.setNoHp(noHp);
    	checkPrepaidBalance.setModuleName(moduleName);
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        EsbCheckPrepaidBalanceResponse checkPrepaidBalanceResponse = restTemplate.postForObject(urlEsbCheckPrepaidBalance, checkPrepaidBalance, EsbCheckPrepaidBalanceResponse.class);
        LgServiceActivities lg;
        lg = new LgServiceActivities(terminalId, transactionId, servlet.getRequestURI(), urlEsbCheckPrepaidBalance, gson.toJson(checkPrepaidBalance), gson.toJson(checkPrepaidBalanceResponse), start, new Timestamp(System.currentTimeMillis()), terminalId, "O");
        loggingService.saveLog(lg);
        return checkPrepaidBalanceResponse;
    }
    
    public EsbCheckPostpaidBalanceResponse getCheckPostpaidBalance(String terminalId, String transactionId, String noHp, HttpServletRequest servlet, String moduleName) throws Exception {
    	EsbCheckPostpaidBalanceRequest checkPostpaidBalance = new EsbCheckPostpaidBalanceRequest();
    	checkPostpaidBalance.setTerminalId(terminalId);
    	checkPostpaidBalance.setTransactionId(transactionId);
    	checkPostpaidBalance.setNoHp(noHp);
    	checkPostpaidBalance.setModuleName(moduleName);
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        EsbCheckPostpaidBalanceResponse checkPostpaidBalanceResponse = restTemplate.postForObject(urlEsbCheckPostpaidBalance, checkPostpaidBalance, EsbCheckPostpaidBalanceResponse.class);
        LgServiceActivities lg;
        lg = new LgServiceActivities(terminalId, transactionId, servlet.getRequestURI(), urlEsbCheckPostpaidBalance, gson.toJson(checkPostpaidBalance), gson.toJson(checkPostpaidBalanceResponse), start, new Timestamp(System.currentTimeMillis()), terminalId, "O");
        loggingService.saveLog(lg);
        return checkPostpaidBalanceResponse;
    }
    
    public EsbServiceProfileResponse getServiceProfile(String terminalId, String transactionId, String noHp, HttpServletRequest servlet, String moduleName) throws Exception {
    	EsbServiceProfileRequest serviceProfile = new EsbServiceProfileRequest();
    	serviceProfile.setTerminalId(terminalId);
    	serviceProfile.setTransactionId(transactionId);
    	serviceProfile.setNoHp(noHp);
    	serviceProfile.setModuleName(moduleName);
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        EsbServiceProfileResponse serviceProfileResponse = restTemplate.postForObject(urlEsbServiceProfile, serviceProfile, EsbServiceProfileResponse.class);
        LgServiceActivities lg;
        lg = new LgServiceActivities(terminalId, transactionId, servlet.getRequestURI(), urlEsbServiceProfile, gson.toJson(serviceProfile), gson.toJson(serviceProfileResponse), start, new Timestamp(System.currentTimeMillis()), terminalId, "O");
        loggingService.saveLog(lg);
        return serviceProfileResponse;
    }
    
    public EsbProductOfferResponse getProductOffer(String terminalId, String transactionId, String noHp, HttpServletRequest servlet, String moduleName, String purchaseMode, String paymentMethod) throws Exception {
    	EsbProductOfferRequest productOffer = new EsbProductOfferRequest();
    	productOffer.setTerminalId(terminalId);
    	productOffer.setTransactionId(transactionId);
    	productOffer.setNoHp(noHp);
    	productOffer.setModuleName(moduleName);
    	productOffer.setPurchaseMode(purchaseMode);
    	productOffer.setPaymentMethod(paymentMethod);
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        EsbProductOfferResponse productOfferResponse = restTemplate.postForObject(urlEsbProductOffer, productOffer, EsbProductOfferResponse.class);
        LgServiceActivities lg;
        lg = new LgServiceActivities(terminalId, transactionId, servlet.getRequestURI(), urlEsbProductOffer, gson.toJson(productOffer), gson.toJson(productOfferResponse), start, new Timestamp(System.currentTimeMillis()), terminalId, "O");
        loggingService.saveLog(lg);
        return productOfferResponse;
    }
    
    public EsbSubmitOrderResponse getSubmitOrder(String terminalId, String transactionId, String noHp, HttpServletRequest servlet, String moduleName, String offerId, String orderType, String purchaseMode, String paymentMethod) throws Exception {
    	EsbSubmitOrderRequest submitOrder = new EsbSubmitOrderRequest();
    	submitOrder.setTerminalId(terminalId);
    	submitOrder.setTransactionId(transactionId);
    	submitOrder.setNoHp(noHp);
    	submitOrder.setModuleName(moduleName);
    	submitOrder.setOfferId(offerId);
    	submitOrder.setOrderType(orderType);
    	submitOrder.setPurchaseMode(purchaseMode);
    	submitOrder.setPaymentMethod(paymentMethod);
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        EsbSubmitOrderResponse submitOrderResponse = restTemplate.postForObject(urlEsbSubmitOrder, submitOrder, EsbSubmitOrderResponse.class);
        LgServiceActivities lg;
        lg = new LgServiceActivities(terminalId, transactionId, servlet.getRequestURI(), urlEsbSubmitOrder, gson.toJson(submitOrder), gson.toJson(submitOrderResponse), start, new Timestamp(System.currentTimeMillis()), terminalId, "O");
        loggingService.saveLog(lg);
        return submitOrderResponse;
    }
}
