package trilogi.myg.svc.pkg.activation.esb;

import trilogi.myg.svc.pkg.activation.model.Account;
import trilogi.myg.svc.pkg.activation.model.Bill;

public class EsbCheckPostpaidBalanceResponse extends EsbResponse{
	private Account account;
	private Bill bill;
	
	public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
}
