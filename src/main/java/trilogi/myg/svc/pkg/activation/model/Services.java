package trilogi.myg.svc.pkg.activation.model;

public class Services {
	private String serviceId;
	private String amount;
	private String expiryDate;
	private String cancelDate;
	private String chargingMode;
	private String productType;
	private String pricePlanId;
	private String pricePlanName;
	private String creditLimitRoaming;
	private String creditLimitDomestic;
	private String activationTime;
	
	public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }
    
    public String getChargingMode() {
        return chargingMode;
    }

    public void setChargingMode(String chargingMode) {
        this.chargingMode = chargingMode;
    }
	
    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }
    
    public String getPricePlanId() {
        return pricePlanId;
    }

    public void setPricePlanId(String pricePlanId) {
        this.pricePlanId = pricePlanId;
    }
    
    public String getPricePlanName() {
        return pricePlanName;
    }

    public void setPricePlanName(String pricePlanName) {
        this.pricePlanName = pricePlanName;
    }
    
    public String getCreditLimitRoaming() {
        return creditLimitRoaming;
    }

    public void setCreditLimitRoaming(String creditLimitRoaming) {
        this.creditLimitRoaming = creditLimitRoaming;
    }
    
    public String getCreditLimitDomestic() {
        return creditLimitDomestic;
    }

    public void setCreditLimitDomestic(String creditLimitDomestic) {
        this.creditLimitDomestic = creditLimitDomestic;
    }
    
    public String getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(String activationTime) {
        this.activationTime = activationTime;
    }
}
