package trilogi.myg.svc.pkg.activation.model;

public class Prices {
	private String payment_method;
	private String value;
	
	public String getPaymentMethod() {
        return payment_method;
    }

    public void setPaymentMethod(String payment_method) {
        this.payment_method = payment_method;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
