package trilogi.myg.svc.pkg.activation;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages={"trilogi.myg.svc.pkg.activation.repository"})
@EnableJpaAuditing
@EntityScan(basePackages = {"trilogi.myg.svc.pkg.activation.entity"})
public class SvcPackageActivationApplication {

	public static void main(String[] args) {
		try {
            String hostname = InetAddress.getLocalHost().getHostName();
            System.out.println(hostname);
            System.setProperty("HostName", InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException er) {
            System.out.println(er.getMessage());
        }
		SpringApplication.run(SvcPackageActivationApplication.class, args);
	}

}
