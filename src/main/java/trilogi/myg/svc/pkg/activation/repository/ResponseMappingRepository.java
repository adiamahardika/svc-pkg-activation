package trilogi.myg.svc.pkg.activation.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import trilogi.myg.svc.pkg.activation.entity.ResponseMapping;

@Repository
public interface ResponseMappingRepository extends CrudRepository<ResponseMapping, Long> {
    ResponseMapping findByEsbCode(String esbCode);
}