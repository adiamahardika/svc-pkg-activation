package trilogi.myg.svc.pkg.activation.logging.service;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import trilogi.myg.svc.pkg.activation.bisnis.model.LogTransactionDetail;
import trilogi.myg.svc.pkg.activation.entity.LgServiceActivities;
import trilogi.myg.svc.pkg.activation.entity.Terminal;
import trilogi.myg.svc.pkg.activation.repository.LgServiceActivitiesRepository;
import trilogi.myg.svc.pkg.activation.repository.TerminalRepository;

@Service("loggingService")
public class LoggingService {

    private final Logger logger = LogManager.getLogger(LoggingService.class);
    private final AtomicLong LAST_TIME_MS = new AtomicLong();
    
    @Autowired
    private LgServiceActivitiesRepository log;
    
    @Autowired
    private TerminalRepository terminalRepository;

    public long getUniqueCurrentTime() {
        long now = System.currentTimeMillis();
        while (true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now) now = lastTime + 1;
            if (LAST_TIME_MS.compareAndSet(lastTime, now)) return now;
        }
    }

    public String getTrxId(String noHandphone, Timestamp time) {
        String timestamp = new SimpleDateFormat("yyMMddHHmmssSSS").format(time);
        String lastMsisdn = noHandphone.substring(noHandphone.length() - 5);
        String a = String.valueOf((int) (Math.random() * 10));
        String trxId = "X002" + timestamp + lastMsisdn + a;
        return trxId;
    }
    
    public void saveLog(LgServiceActivities logService) {
        logger.info("save log");
        logService.setLogDate(new Timestamp(this.getUniqueCurrentTime()));
        CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                try {
                    logger.info("save log");
                    log.save(logService);
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    logger.error("ERROR Save Log : {}", e.getMessage());
                }
            }
        });
    }
    
    public void createLogDetail(LogTransactionDetail detail) {
        Terminal t = terminalRepository.findByTerminalIdAndStatus(detail.getTerminalId(), "A");
        CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                String hostname = "";
                try {
                    hostname = InetAddress.getLocalHost().getHostName();
                } catch(UnknownHostException er) {
                    System.out.println(er.getMessage());
                }

                try {
                    String yyyyMMdd = new SimpleDateFormat("yyyyMMdd").format(new Date());
                    String fileName = "/var/log/grapari/api_trilogi_" + hostname + "_" + yyyyMMdd + ".log";

                    detail.setHostname(hostname);
                    detail.setMitra("Trilogi");
                    detail.setArea(t != null ? t.getArea() : "");

                    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
                    writer.newLine();
                    writer.write(detail.toString());
                    writer.close();
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (Exception er) {
                    er.printStackTrace();
                }
            }
        });
    }
    
}