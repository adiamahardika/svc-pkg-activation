package trilogi.myg.svc.pkg.activation.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import trilogi.myg.svc.pkg.activation.entity.LgPayment;

@Repository
public interface LgPaymentRepository extends CrudRepository<LgPayment, Long> {
    LgPayment findByTransactionIdAndNoHp(String transactionId, String noHp);
}
