package trilogi.myg.svc.pkg.activation.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "ms_response_mapping")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResponseMapping {
    @GenericGenerator(
        name = "msResponseMappingIdCodeSeqGenerator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "msResponseMappingIdCodeSeq"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
        }
    )

    @Id
    @GeneratedValue(generator = "msResponseMappingIdCodeSeqGenerator")
    @JsonIgnore
    @Column(name="id_code")
    private Long id;

    @Column(name = "esb_code")
    private String esbCode;

    @Column(name = "esb_http_code")
    private String esbHttpCode;

    @Column(name = "mygrapari_code")
    private String mygrapariCode;

    @Column(name = "response_code")
    private String responseCode;

    @Column(name = "message_id")
    private String messageId;

    @Column(name = "message_en")
    private String messageEn;

    @Column(name = "message_esb")
    private String messageEsb;

    @Column(name = "retry")
    private String retry;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEsbCode() {
        return esbCode;
    }

    public void setEsbCode(String esbCode) {
        this.esbCode = esbCode;
    }

    public String getMygrapariCode() {
        return mygrapariCode;
    }

    public void setMygrapariCode(String mygrapariCode) {
        this.mygrapariCode = mygrapariCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageEn() {
        return messageEn;
    }

    public void setMessageEn(String messageEn) {
        this.messageEn = messageEn;
    }

    public String getMessageEsb() {
        return messageEsb;
    }

    public void setMessageEsb(String messageEsb) {
        this.messageEsb = messageEsb;
    }

    public String getRetry() {
        return retry;
    }

    public void setRetry(String retry) {
        this.retry = retry;
    }

    public String getEsbHttpCode() {
        return esbHttpCode;
    }

    public void setEsbHttpCode(String esbHttpCode) {
        this.esbHttpCode = esbHttpCode;
    }
}
