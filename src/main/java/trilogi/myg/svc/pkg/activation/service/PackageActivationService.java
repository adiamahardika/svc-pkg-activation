package trilogi.myg.svc.pkg.activation.service;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import trilogi.myg.svc.pkg.activation.bisnis.model.LogTransactionDetail;
import trilogi.myg.svc.pkg.activation.bisnis.paket.GetPaketRequest;
import trilogi.myg.svc.pkg.activation.bisnis.paket.GetPaketResponse;
import trilogi.myg.svc.pkg.activation.bisnis.payment.PaymentRequest;
import trilogi.myg.svc.pkg.activation.bisnis.payment.PaymentResponse;
import trilogi.myg.svc.pkg.activation.bisnis.profile.CustomerProfileRequest;
import trilogi.myg.svc.pkg.activation.bisnis.profile.CustomerProfileResponse;
import trilogi.myg.svc.pkg.activation.bisnis.profile.Profile;
import trilogi.myg.svc.pkg.activation.constant.GeneralConstants;
import trilogi.myg.svc.pkg.activation.entity.CustomerTransaction;
import trilogi.myg.svc.pkg.activation.entity.LgPayment;
import trilogi.myg.svc.pkg.activation.entity.LgServiceActivities;
import trilogi.myg.svc.pkg.activation.entity.ResponseMapping;
import trilogi.myg.svc.pkg.activation.entity.Terminal;
import trilogi.myg.svc.pkg.activation.esb.EsbCheckPostpaidBalanceResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbCheckPrepaidBalanceResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbCustomerProfileResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbNetworkProfileResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbProductOfferResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbServiceProfileResponse;
import trilogi.myg.svc.pkg.activation.esb.EsbSubmitOrderResponse;
import trilogi.myg.svc.pkg.activation.esb.service.EsbIntegrationService;
import trilogi.myg.svc.pkg.activation.logging.service.LoggingService;
import trilogi.myg.svc.pkg.activation.model.Account;
import trilogi.myg.svc.pkg.activation.model.Bill;
import trilogi.myg.svc.pkg.activation.model.InitRequest;
import trilogi.myg.svc.pkg.activation.model.InitResponse;
import trilogi.myg.svc.pkg.activation.model.NetworkProfile;
import trilogi.myg.svc.pkg.activation.model.Offer;
import trilogi.myg.svc.pkg.activation.model.Services;
import trilogi.myg.svc.pkg.activation.repository.CustomerTransactionRepository;
import trilogi.myg.svc.pkg.activation.repository.LgPaymentRepository;
import trilogi.myg.svc.pkg.activation.repository.ResponseMappingRepository;
import trilogi.myg.svc.pkg.activation.repository.TerminalRepository;


@Service("packageActivationService")
@RefreshScope
public class PackageActivationService {
	
	private final Logger logger = LogManager.getLogger(PackageActivationService.class);
	private static final String MODULE_NAME = "";
	private static final String ORDER_TYPE = "ACT";
	
	@Autowired
    private Gson gson;

	@Autowired
    private TerminalRepository terminalRepo;
	
	@Autowired
    private LoggingService loggingService;
	
    @Autowired
    private EsbIntegrationService esbIntegrationService;
    
    @Autowired
    private ResponseMappingRepository responseRepository;
    
    @Autowired
    private CustomerTransactionRepository customerRepository;
    
    @Autowired
    private LgPaymentRepository paymentRepository;
	
	@Value("${foto.selfie.folder}")
    private String fotoSelfieFolder;
	
	@Value("${payment.method.prepaid}")
    private String paymentMethodPrepaid;
	
	@Value("${payment.method.postpaid}")
    private String paymentMethodPostpaid;
	
	@Value("${purchace.method}")
    private String purchaseMethod;

	public ResponseEntity<Object> init(InitRequest request) {
        InitResponse response = new InitResponse();
        try {
            Terminal terminal = terminalRepo.findByTerminalIdAndStatus(request.getTerminalId(), GeneralConstants.AKTIF);
            if(terminal !=  null) {
                response.setTerminal(terminal);
                response.setConfig(terminal.getConfig());
                response.getTransaction().setStatusCode(GeneralConstants.RC_SUCCESS);
                response.getTransaction().setStatusDesc(GeneralConstants.RC_SUCCESS_MESSAGE);
            } else {
                response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
                response.getTransaction().setStatusDesc("Terminal Id Tidak Ditemukan");
            }
        } catch (Exception error) {
            if(error instanceof DataAccessException) {
                response.getTransaction().setErrorCode("50300");
            }
            response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
            response.getTransaction().setStatusDesc(error.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	public ResponseEntity<Object> submitNoHp(CustomerProfileRequest request, HttpServletRequest servletRequest) {
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        String logId = request.getTerminalId() + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(start.getTime());
        String trxId = loggingService.getTrxId(request.getNoHp(), start);

        CustomerProfileResponse response = new CustomerProfileResponse();
        response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
        response.getTransaction().setTransactionId(trxId);
        String res = "";
        LgServiceActivities lg;
        ResponseMapping rm = null;
        try {
            File outputFile = new File(fotoSelfieFolder + trxId + ".jpg");
            byte[] decodedBytes = Base64.getDecoder().decode(request.getImage());
            FileUtils.writeByteArrayToFile(outputFile, decodedBytes);
            request.setImage(outputFile.getName());
        } catch (IOException io) {
            logger.error(io.getMessage());
        } catch (NullPointerException nullError) {
            logger.error(nullError.getMessage());
        }
        
        EsbCustomerProfileResponse customerProfileResponse = new EsbCustomerProfileResponse();;
        EsbNetworkProfileResponse networkProfileResponse = new EsbNetworkProfileResponse();
        
        try {
            customerProfileResponse = esbIntegrationService.getCustomerProfile(request.getTerminalId(), trxId, request.getNoHp(), servletRequest, MODULE_NAME);
            networkProfileResponse = esbIntegrationService.getNetworkProfile(request.getTerminalId(), trxId, request.getNoHp(), servletRequest, MODULE_NAME);

            CustomerTransaction customerTransaction = new CustomerTransaction();
            customerTransaction.setTransactionId(trxId);
            customerTransaction.setLogId(logId);
            customerTransaction.setNoHp(request.getNoHp());
            customerTransaction.setCreatedBy(request.getTerminalId());
            customerTransaction.setCreatedDate(start);
            customerTransaction.setCustomerName(GeneralConstants.NO_DATA);
            customerTransaction.setFinancialAccountId(GeneralConstants.NO_DATA);
            customerTransaction.setBrand(GeneralConstants.NO_DATA);

            if(customerProfileResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
                customerTransaction.setCustomerName(customerProfileResponse.getName());
                customerTransaction.setFinancialAccountId(customerProfileResponse.getFinancialAccountId());
                customerTransaction.setBrand(customerProfileResponse.getBrand());
                customerTransaction.setType(networkProfileResponse.getNetworkProfile().getSubscriberType());
                
                response.getTransaction().setStatusCode(GeneralConstants.RC_SUCCESS);
                response.getTransaction().setStatusDesc(GeneralConstants.RC_SUCCESS_MESSAGE);
                
                Profile profile = new Profile();
                profile.setIdNumber(customerProfileResponse.getIdNumber() == null ? "" : customerProfileResponse.getIdNumber());
                profile.setIdType(customerProfileResponse.getIdType() == null ? "" : customerProfileResponse.getIdType());
                profile.setNama(customerProfileResponse.getName());
                profile.setNoHp(customerProfileResponse.getNoHp());
                profile.setType(networkProfileResponse.getNetworkProfile().getSubscriberType());
                profile.setFinancialAccountId(customerProfileResponse.getFinancialAccountId());
                profile.setBrand(customerProfileResponse.getBrand());
                response.setProfile(profile);
            } else {
                rm = responseRepository.findByEsbCode(customerProfileResponse.getErrorCode());
                if (rm != null) {
                    response.getTransaction().setStatusCode(rm.getResponseCode());
                    response.getTransaction().setStatusDesc(rm.getMessageId());
                } else {
                    response.getTransaction().setStatusCode(customerProfileResponse.getResponseCode());
                    response.getTransaction().setStatusDesc(customerProfileResponse.getResponseMessage());
                }
                response.getTransaction().setErrorCode(customerProfileResponse.getErrorCode());
            }
            customerRepository.save(customerTransaction);
            ObjectMapper mapper = new ObjectMapper();
            res = mapper.writeValueAsString(response);
        } catch (Exception error) {
            response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
            response.getTransaction().setStatusDesc(error.getMessage());
            res = gson.toJson(response);
            lg = new LgServiceActivities(request.getTerminalId(), trxId, request.getTerminalId(), servletRequest.getRequestURI(), gson.toJson(request), res, start, new Timestamp(System.currentTimeMillis()), request.getTerminalId(), "I");
            loggingService.saveLog(lg);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        lg = new LgServiceActivities(request.getTerminalId(), trxId, request.getTerminalId(), servletRequest.getRequestURI(), gson.toJson(request), res, start, new Timestamp(System.currentTimeMillis()), request.getTerminalId(), "I");
        loggingService.saveLog(lg);

        LogTransactionDetail ltd = new LogTransactionDetail();
        ltd.setDatetime(new SimpleDateFormat("yyyyMMddHHmmss").format(start.getTime()));
        ltd.setIpAddress(servletRequest.getHeader("X-Forwarded-For").split(",")[0]);
        ltd.setLogId(logId);
        ltd.setTerminalId(request.getTerminalId());
        ltd.setEvent("PB");
        ltd.setStep("1");
        ltd.setNoHp(request.getNoHp());
        ltd.setUrl(servletRequest.getRequestURI());
        ltd.setResponse(gson.toJson(response));
        ltd.setResponseStatusCode(response.getTransaction().getStatusCode());
        ltd.setUrlEsb(customerProfileResponse == null ? "" : customerProfileResponse.getUrl());
        ltd.setBackendRequestTime(new SimpleDateFormat("yyyyMMddHHmmss").format(customerProfileResponse.getRequestTime()));
        ltd.setEsbResponse(customerProfileResponse == null ? "" : gson.toJson(customerProfileResponse.getDataEsb()));
        ltd.setEsbHttpStatusCode(customerProfileResponse == null ? "" : customerProfileResponse.getHttpStatusEsb());
        ltd.setEsbResponseTime(customerProfileResponse == null ? "" : String.valueOf(customerProfileResponse.getResponseTime() - customerProfileResponse.getRequestTime()));
        ltd.setResponseTime(String.valueOf(loggingService.getUniqueCurrentTime() - start.getTime()));
        loggingService.createLogDetail(ltd);

        if(rm != null) {
            return mappingError(response, rm.getEsbHttpCode());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	public ResponseEntity<Object> getPaket(GetPaketRequest request, HttpServletRequest servletRequest) {
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        String logId = request.getTerminalId() + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(start.getTime());

        GetPaketResponse response = new GetPaketResponse();
        response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
        response.getTransaction().setTransactionId(request.getTransactionId());
        String res = "";
        LgServiceActivities lg;
        ResponseMapping rm = null;
        
        EsbCheckPrepaidBalanceResponse checkPrepaidBalanceResponse = new EsbCheckPrepaidBalanceResponse();
        EsbCheckPostpaidBalanceResponse checkPostpaidBalanceResponse = new EsbCheckPostpaidBalanceResponse();
        EsbServiceProfileResponse serviceProfileResponse = new EsbServiceProfileResponse();
        EsbProductOfferResponse productOfferResponse = new EsbProductOfferResponse(); 
        try {
        			 
        	if(request.getType().equalsIgnoreCase(GeneralConstants.PREPAID_TYPE)) {
        		productOfferResponse = esbIntegrationService.getProductOffer(request.getTerminalId(), request.getTransactionId(), request.getNoHp(), servletRequest, MODULE_NAME, purchaseMethod, paymentMethodPrepaid);
        		
        		if(productOfferResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)){
        			checkPrepaidBalanceResponse = esbIntegrationService.getCheckPrepaidBalance(request.getTerminalId(), request.getTransactionId(), request.getNoHp(), servletRequest, MODULE_NAME);

                    if(checkPrepaidBalanceResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
                        
                        response.getTransaction().setStatusCode(GeneralConstants.RC_SUCCESS);
                        response.getTransaction().setStatusDesc(GeneralConstants.RC_SUCCESS_MESSAGE);
                        
                        Services service = new Services();
                        service.setAmount(checkPrepaidBalanceResponse.getAmount());
                        service.setCancelDate(checkPrepaidBalanceResponse.getService().getCancelDate());
                        service.setChargingMode(checkPrepaidBalanceResponse.getService().getChargingMode());
                        service.setExpiryDate(checkPrepaidBalanceResponse.getService().getExpiryDate());
                        service.setProductType(checkPrepaidBalanceResponse.getService().getProductType());
                        service.setServiceId(checkPrepaidBalanceResponse.getService().getServiceId());
                        service.setPricePlanId(GeneralConstants.NO_DATA);
                        service.setPricePlanName(GeneralConstants.NO_DATA);
                        service.setCreditLimitDomestic(GeneralConstants.NO_DATA);
                        service.setCreditLimitRoaming(GeneralConstants.NO_DATA);
                        service.setActivationTime(GeneralConstants.NO_DATA);
                        response.setService(service);
                        
                        NetworkProfile networkProfile =  new NetworkProfile();
                        networkProfile.setScpId(checkPrepaidBalanceResponse.getNetworkProfile().getScpId());
                        networkProfile.setSubscriberType(request.getType());
                        response.setNetworkProfile(networkProfile);
                        
                        Bill bill = new Bill();
                        bill.setServiceId(GeneralConstants.NO_DATA);
                        bill.setBalance(GeneralConstants.NO_DATA);
                        response.setBill(bill);
                        
                        Account account = new Account();
                        account.setAccountId(GeneralConstants.NO_DATA);
                        response.setAccount(account);
                        
                        String confirmationMessage = new String();
                        confirmationMessage = productOfferResponse.getConfirmationMessage();
                        response.setConfirmationMessage(confirmationMessage);
                        
                        List<Offer> offer = new ArrayList<>();
                        for (int offerIndex = 0; offerIndex < productOfferResponse.getOffer().size(); offerIndex++) {
                            if(!productOfferResponse.getOffer().get(offerIndex).getOfferId().equalsIgnoreCase("")) {
                            	offer.add(productOfferResponse.getOffer().get(offerIndex));
                            }
                        }
                        response.setOffer(offer);
                        
                    } else {
                        rm = responseRepository.findByEsbCode(checkPrepaidBalanceResponse.getErrorCode());
                        if (rm != null) {
                            response.getTransaction().setStatusCode(rm.getResponseCode());
                            response.getTransaction().setStatusDesc(rm.getMessageId());
                        } else {
                            response.getTransaction().setStatusCode(checkPrepaidBalanceResponse.getResponseCode());
                            response.getTransaction().setStatusDesc(checkPrepaidBalanceResponse.getResponseMessage());
                        }
                        response.getTransaction().setErrorCode(checkPrepaidBalanceResponse.getErrorCode());
                    }
        		} else {
        			rm = responseRepository.findByEsbCode(productOfferResponse.getErrorCode());
                    if (rm != null) {
                        response.getTransaction().setStatusCode(rm.getResponseCode());
                        response.getTransaction().setStatusDesc(rm.getMessageId());
                    } else {
                        response.getTransaction().setStatusCode(productOfferResponse.getResponseCode());
                        response.getTransaction().setStatusDesc(productOfferResponse.getResponseMessage());
                    }
                    response.getTransaction().setErrorCode(productOfferResponse.getErrorCode());
        		}
        		
        	} else if (request.getType().equalsIgnoreCase(GeneralConstants.POSTPAID_TYPE)) {
        		
        		productOfferResponse = esbIntegrationService.getProductOffer(request.getTerminalId(), request.getTransactionId(), request.getNoHp(), servletRequest, MODULE_NAME, purchaseMethod, paymentMethodPostpaid);
        		
        		if(productOfferResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)){
        			
        			serviceProfileResponse = esbIntegrationService.getServiceProfile(request.getTerminalId(), request.getTransactionId(), request.getNoHp(), servletRequest, MODULE_NAME);
        			
            		if(serviceProfileResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
            				
            				checkPostpaidBalanceResponse = esbIntegrationService.getCheckPostpaidBalance(request.getTerminalId(), request.getTransactionId(), request.getNoHp(), servletRequest, MODULE_NAME);
            				
            				if(checkPostpaidBalanceResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
            					
            					response.getTransaction().setStatusCode(GeneralConstants.RC_SUCCESS);
                                response.getTransaction().setStatusDesc(GeneralConstants.RC_SUCCESS_MESSAGE);
                                
                                Services service = new Services();
                                service.setAmount(GeneralConstants.NO_DATA);
                                service.setCancelDate(GeneralConstants.NO_DATA);
                                service.setChargingMode(GeneralConstants.NO_DATA);
                                service.setExpiryDate(GeneralConstants.NO_DATA);
                                service.setProductType(GeneralConstants.NO_DATA);
                                service.setServiceId(serviceProfileResponse.getService().getServiceId());
                                service.setPricePlanId(serviceProfileResponse.getService().getPricePlanId());
                                service.setPricePlanName(serviceProfileResponse.getService().getPricePlanName());
                                service.setCreditLimitDomestic(serviceProfileResponse.getService().getCreditLimitDomestic());
                                service.setCreditLimitRoaming(serviceProfileResponse.getService().getCreditLimitRoaming());
                                service.setActivationTime(serviceProfileResponse.getService().getActivationTime());
                                response.setService(service);
                                
                                NetworkProfile networkProfile =  new NetworkProfile();
                                networkProfile.setScpId(GeneralConstants.NO_DATA);
                                networkProfile.setSubscriberType(request.getType());
                                response.setNetworkProfile(networkProfile);
                                
                                Bill bill = new Bill();
                                bill.setServiceId(checkPostpaidBalanceResponse.getBill().getServiceId());
                                bill.setBalance(checkPostpaidBalanceResponse.getBill().getBalance());
                                response.setBill(bill);
                                
                                Account account = new Account();
                                account.setAccountId(checkPostpaidBalanceResponse.getAccount().getAccountId());
                                response.setAccount(account);
                                
                                response.setConfirmationMessage(productOfferResponse.getConfirmationMessage());
                                
                                List<Offer> offer = new ArrayList<>();
                                for (int offerIndex = 0; offerIndex < productOfferResponse.getOffer().size(); offerIndex++) {
                                    if(!productOfferResponse.getOffer().get(offerIndex).getOfferId().equalsIgnoreCase("")) {
                                    	offer.add(productOfferResponse.getOffer().get(offerIndex));
                                    }
                                }
                                response.setOffer(offer);
                                
            				} else {
            					rm = responseRepository.findByEsbCode(checkPostpaidBalanceResponse.getErrorCode());
                                if (rm != null) {
                                    response.getTransaction().setStatusCode(rm.getResponseCode());
                                    response.getTransaction().setStatusDesc(rm.getMessageId());
                                } else {
                                    response.getTransaction().setStatusCode(checkPostpaidBalanceResponse.getResponseCode());
                                    response.getTransaction().setStatusDesc(checkPostpaidBalanceResponse.getResponseMessage());
                                }
                                response.getTransaction().setErrorCode(checkPostpaidBalanceResponse.getErrorCode());
            				}
                        
                    } else {
                        rm = responseRepository.findByEsbCode(serviceProfileResponse.getErrorCode());
                        if (rm != null) {
                            response.getTransaction().setStatusCode(rm.getResponseCode());
                            response.getTransaction().setStatusDesc(rm.getMessageId());
                        } else {
                            response.getTransaction().setStatusCode(serviceProfileResponse.getResponseCode());
                            response.getTransaction().setStatusDesc(serviceProfileResponse.getResponseMessage());
                        }
                        response.getTransaction().setErrorCode(serviceProfileResponse.getErrorCode());
                    }
        		} else {
        			rm = responseRepository.findByEsbCode(productOfferResponse.getErrorCode());
                    if (rm != null) {
                        response.getTransaction().setStatusCode(rm.getResponseCode());
                        response.getTransaction().setStatusDesc(rm.getMessageId());
                    } else {
                        response.getTransaction().setStatusCode(productOfferResponse.getResponseCode());
                        response.getTransaction().setStatusDesc(productOfferResponse.getResponseMessage());
                    }
                    response.getTransaction().setErrorCode(productOfferResponse.getErrorCode());
        		}
        	} else {
                response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
                response.getTransaction().setStatusDesc("Type Not Found!");
        	}
        	ObjectMapper mapper = new ObjectMapper();
            res = mapper.writeValueAsString(response);
        } catch (Exception error) {
            response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
            response.getTransaction().setStatusDesc(error.getMessage());
            res = gson.toJson(response);
            lg = new LgServiceActivities(request.getTerminalId(), request.getTransactionId(), request.getTerminalId(), servletRequest.getRequestURI(), gson.toJson(request), res, start, new Timestamp(System.currentTimeMillis()), request.getTerminalId(), "I");
            loggingService.saveLog(lg);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        lg = new LgServiceActivities(request.getTerminalId(), request.getTransactionId(), request.getTerminalId(), servletRequest.getRequestURI(), gson.toJson(request), res, start, new Timestamp(System.currentTimeMillis()), request.getTerminalId(), "I");
        loggingService.saveLog(lg);

        LogTransactionDetail ltd = new LogTransactionDetail();
        ltd.setDatetime(new SimpleDateFormat("yyyyMMddHHmmss").format(start.getTime()));
        ltd.setIpAddress(servletRequest.getHeader("X-Forwarded-For").split(",")[0]);
        ltd.setLogId(logId);
        ltd.setTerminalId(request.getTerminalId());
        ltd.setEvent("PB");
        ltd.setStep("1");
        ltd.setNoHp(request.getNoHp());
        ltd.setUrl(servletRequest.getRequestURI());
        ltd.setResponse(gson.toJson(response));
        ltd.setResponseStatusCode(response.getTransaction().getStatusCode());
        if (request.getType().equalsIgnoreCase(GeneralConstants.PREPAID_TYPE)) {
            ltd.setUrlEsb(checkPrepaidBalanceResponse == null ? "" : checkPrepaidBalanceResponse.getUrl());
            ltd.setBackendRequestTime(new SimpleDateFormat("yyyyMMddHHmmss").format(checkPrepaidBalanceResponse.getRequestTime()));
            ltd.setEsbResponse(checkPrepaidBalanceResponse == null ? "" : gson.toJson(checkPrepaidBalanceResponse.getDataEsb()));
            ltd.setEsbHttpStatusCode(checkPrepaidBalanceResponse == null ? "" : checkPrepaidBalanceResponse.getHttpStatusEsb());
            ltd.setEsbResponseTime(checkPrepaidBalanceResponse == null ? "" : String.valueOf(checkPrepaidBalanceResponse.getResponseTime() - checkPrepaidBalanceResponse.getRequestTime()));
            ltd.setResponseTime(String.valueOf(loggingService.getUniqueCurrentTime() - start.getTime()));      
        } else if (request.getType().equalsIgnoreCase(GeneralConstants.POSTPAID_TYPE)) {
            ltd.setUrlEsb(checkPostpaidBalanceResponse == null ? "" : checkPostpaidBalanceResponse.getUrl());
            ltd.setBackendRequestTime(new SimpleDateFormat("yyyyMMddHHmmss").format(checkPostpaidBalanceResponse.getRequestTime()));
            ltd.setEsbResponse(checkPostpaidBalanceResponse == null ? "" : gson.toJson(checkPostpaidBalanceResponse.getDataEsb()));
            ltd.setEsbHttpStatusCode(checkPostpaidBalanceResponse == null ? "" : checkPostpaidBalanceResponse.getHttpStatusEsb());
            ltd.setEsbResponseTime(checkPostpaidBalanceResponse == null ? "" : String.valueOf(checkPostpaidBalanceResponse.getResponseTime() - checkPostpaidBalanceResponse.getRequestTime()));
            ltd.setResponseTime(String.valueOf(loggingService.getUniqueCurrentTime() - start.getTime()));
            loggingService.createLogDetail(ltd);           
        }
        loggingService.createLogDetail(ltd);

        if(rm != null) {
            return mappingError(response, rm.getEsbHttpCode());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	public ResponseEntity<Object> doPayment(PaymentRequest request, HttpServletRequest servletRequest) {
        Timestamp start = new Timestamp(loggingService.getUniqueCurrentTime());
        String logId = request.getTerminalId() + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(start.getTime());
        
        PaymentResponse response = new PaymentResponse();
        response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
        response.getTransaction().setTransactionId(request.getTransactionId());

        String res = "";
        ResponseMapping rm = null;
        LgServiceActivities lg;
        
        EsbSubmitOrderResponse submitOrderResponse = null;
        
        try {
            Terminal terminal = terminalRepo.findByTerminalIdAndStatus(request.getTerminalId(), GeneralConstants.AKTIF);
            CustomerTransaction csTransaction = customerRepository.findByTransactionId(request.getTransactionId());
            if(terminal != null && csTransaction != null) {
                //save to lg payment
                LgPayment lgPayment = new LgPayment();
                lgPayment.setNoHp(request.getNoHp());
                lgPayment.setAmount(Double.parseDouble(request.getAmount()));
                lgPayment.setSubmitAmount(Double.parseDouble(request.getSubmitAmount()));
                lgPayment.setPaymentMethod(request.getType() == GeneralConstants.PREPAID_TYPE ? paymentMethodPrepaid : paymentMethodPostpaid);
                lgPayment.setFaAccountId(csTransaction == null ? "-" : csTransaction.getFinancialAccountId());
                lgPayment.setCreatedBy(request.getTerminalId());
                lgPayment.setCreatedDate(new Timestamp(loggingService.getUniqueCurrentTime()));
                lgPayment.setTransactionId(request.getTransactionId());
                lgPayment.setEcr("-");
                lgPayment.setResponseCode(request.getResponseCode());
                if(request.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
                    if(Double.parseDouble(request.getSubmitAmount()) >= Double.parseDouble(request.getAmount())) {
                    	lgPayment.setStatus(GeneralConstants.PAID);
	                } else {
	                	lgPayment.setStatus(GeneralConstants.CANCELED);
	                }
                } else {
                	lgPayment.setStatus(GeneralConstants.CANCELED);
                }

                LgPayment lgPaymentRepository = paymentRepository.save(lgPayment);
                
                if(lgPaymentRepository != null) {
                	if(Double.parseDouble(request.getSubmitAmount()) >= Double.parseDouble(request.getAmount())) {
                		if(request.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
                			
                			if(request.getType().equalsIgnoreCase(GeneralConstants.PREPAID_TYPE)) {
                				submitOrderResponse = esbIntegrationService.getSubmitOrder(request.getTerminalId(), request.getTransactionId(), request.getNoHp(), servletRequest, MODULE_NAME, request.getOfferId(), ORDER_TYPE, purchaseMethod, paymentMethodPrepaid);
                				
                				if (submitOrderResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
                            		response.getTransaction().setStatusCode(GeneralConstants.RC_SUCCESS);
                                    response.getTransaction().setStatusDesc(GeneralConstants.RC_SUCCESS_MESSAGE);
                                    
                                    response.setIseligible(submitOrderResponse.getIseligible());
                                    response.setNotification(submitOrderResponse.getNotification());
                                    response.setServiceIdA(submitOrderResponse.getServiceIdA());
        						} else {
        							rm = responseRepository.findByEsbCode(submitOrderResponse.getErrorCode());
        			                if (rm != null) {
        			                    response.getTransaction().setStatusCode(rm.getResponseCode());
        			                    response.getTransaction().setStatusDesc(rm.getMessageId());
        			                } else {
        			                    response.getTransaction().setStatusCode(submitOrderResponse.getResponseCode());
        			                    response.getTransaction().setStatusDesc(submitOrderResponse.getResponseMessage());
        			                }
        			                response.getTransaction().setErrorCode(submitOrderResponse.getErrorCode());
        						}
                			} else if (request.getType().equalsIgnoreCase(GeneralConstants.POSTPAID_TYPE)) {
                				submitOrderResponse = esbIntegrationService.getSubmitOrder(request.getTerminalId(), request.getTransactionId(), request.getNoHp(), servletRequest, MODULE_NAME, request.getOfferId(), ORDER_TYPE, purchaseMethod, paymentMethodPostpaid);
                				
                				if (submitOrderResponse.getResponseCode().equalsIgnoreCase(GeneralConstants.RC_SUCCESS)) {
                            		response.getTransaction().setStatusCode(GeneralConstants.RC_SUCCESS);
                                    response.getTransaction().setStatusDesc(GeneralConstants.RC_SUCCESS_MESSAGE);
                                    
                                    response.setIseligible(submitOrderResponse.getIseligible());
                                    response.setNotification(submitOrderResponse.getNotification());
                                    response.setServiceIdA(submitOrderResponse.getServiceIdA());
        						} else {
        							rm = responseRepository.findByEsbCode(submitOrderResponse.getErrorCode());
        			                if (rm != null) {
        			                    response.getTransaction().setStatusCode(rm.getResponseCode());
        			                    response.getTransaction().setStatusDesc(rm.getMessageId());
        			                } else {
        			                    response.getTransaction().setStatusCode(submitOrderResponse.getResponseCode());
        			                    response.getTransaction().setStatusDesc(submitOrderResponse.getResponseMessage());
        			                }
        			                response.getTransaction().setErrorCode(submitOrderResponse.getErrorCode());
        						}
                			}
                        } else {
                            response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
                            response.getTransaction().setStatusDesc("");
                            response.getTransaction().setTransactionId(request.getTransactionId());
                            response.getTransaction().setErrorCode("99009");
                        }
                	} else {
                		response.getTransaction().setStatusCode(GeneralConstants.RC_WARNING);
                        response.getTransaction().setStatusDesc("");
                        response.setIseligible("false");
                        response.setNotification("Sisa saldo Anda tidak mencukupi untuk membeli Paket Internet. Silakan melakukan isi ulang.");
                        response.setServiceIdA(request.getNoHp());
                	}
                }
                ObjectMapper mapper = new ObjectMapper();
                res = mapper.writeValueAsString(response);
            }
        } catch (Exception error) {
            error.printStackTrace();
            logger.info("Exception : {}", error.getMessage());
            response.getTransaction().setStatusCode(GeneralConstants.RC_EXCEPTION);
            response.getTransaction().setStatusDesc(error.getMessage());
            res = gson.toJson(response);
        }

        lg = new LgServiceActivities(request.getTerminalId(), request.getTransactionId(), request.getTerminalId(), servletRequest.getRequestURI(), gson.toJson(request), res, start, new Timestamp(System.currentTimeMillis()), request.getTerminalId(), "I");

        loggingService.saveLog(lg);
        LogTransactionDetail ltd = new LogTransactionDetail();
        ltd.setDatetime(new SimpleDateFormat("yyyyMMddHHmmss").format(start.getTime()));
        ltd.setIpAddress(servletRequest.getHeader("X-Forwarded-For").split(",")[0]);
        ltd.setLogId(logId);
        ltd.setTerminalId(request.getTerminalId());
        ltd.setEvent("PB");
        ltd.setStep("3");
        ltd.setNoHp(request.getNoHp());
        ltd.setUrl(servletRequest.getRequestURI());
        ltd.setResponse(gson.toJson(response));
        ltd.setResponseStatusCode(response.getTransaction().getStatusCode());
        ltd.setUrlEsb(submitOrderResponse == null ? "" : submitOrderResponse.getUrl());
        ltd.setBackendRequestTime(submitOrderResponse == null ? "" : new SimpleDateFormat("yyyyMMddHHmmss").format(submitOrderResponse.getRequestTime()));
        ltd.setEsbResponse(submitOrderResponse == null ? "" : gson.toJson(submitOrderResponse.getDataEsb()));
        ltd.setEsbHttpStatusCode(submitOrderResponse == null ? "" : submitOrderResponse.getHttpStatusEsb());
        ltd.setEsbResponseTime(submitOrderResponse == null ? "" : String.valueOf(submitOrderResponse.getResponseTime() - submitOrderResponse.getRequestTime()));
        ltd.setResponseTime(String.valueOf(loggingService.getUniqueCurrentTime() - start.getTime()));
        loggingService.createLogDetail(ltd);

        if(rm != null) {
            return mappingError(response, rm.getEsbHttpCode());
        }
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	private ResponseEntity<Object> mappingError(Object a, String status) {
        int s = Integer.parseInt(status);
        switch (s) {
            case 400 :
                return new ResponseEntity<>(a, HttpStatus.BAD_REQUEST);
            case 500 :
                return new ResponseEntity<>(a, HttpStatus.INTERNAL_SERVER_ERROR);
            default: return  new ResponseEntity<>(a, HttpStatus.OK);
        }
    }
}
