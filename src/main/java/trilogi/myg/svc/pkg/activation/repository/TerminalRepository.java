package trilogi.myg.svc.pkg.activation.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import trilogi.myg.svc.pkg.activation.entity.Terminal;

@Repository
public interface TerminalRepository extends CrudRepository<Terminal, Long> {
    Terminal findByTerminalIdAndStatus(String terminalId, String Status);
}
