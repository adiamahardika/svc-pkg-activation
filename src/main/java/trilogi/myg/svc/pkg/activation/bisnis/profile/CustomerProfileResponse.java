package trilogi.myg.svc.pkg.activation.bisnis.profile;

import com.fasterxml.jackson.annotation.JsonInclude;

import trilogi.myg.svc.pkg.activation.constant.StandardResponse;
import trilogi.myg.svc.pkg.activation.entity.VoucherPulsa;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerProfileResponse extends StandardResponse {

	private static final long serialVersionUID = 6536449038288806954L;
	
	private Profile profile;
    private Double tagihan;
    private List<VoucherPulsa> listPulsa;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Double getTagihan() {
        return tagihan;
    }

    public void setTagihan(Double tagihan) {
        this.tagihan = tagihan;
    }

    public List<VoucherPulsa> getListPulsa() {
        return listPulsa;
    }

    public void setListPulsa(List<VoucherPulsa> listPulsa) {
        this.listPulsa = listPulsa;
    }
}
