package trilogi.myg.svc.pkg.activation.repository;

import org.springframework.data.repository.CrudRepository;

import trilogi.myg.svc.pkg.activation.entity.CustomerTransaction;

public interface CustomerTransactionRepository extends CrudRepository<CustomerTransaction, Long> {
    CustomerTransaction findByTransactionId(String transactionId);
}
