package trilogi.myg.svc.pkg.activation.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import trilogi.myg.svc.pkg.activation.bisnis.paket.GetPaketRequest;
import trilogi.myg.svc.pkg.activation.bisnis.payment.PaymentRequest;
import trilogi.myg.svc.pkg.activation.bisnis.profile.CustomerProfileRequest;
import trilogi.myg.svc.pkg.activation.general.EncryptionValidationService;
import trilogi.myg.svc.pkg.activation.model.InitRequest;
import trilogi.myg.svc.pkg.activation.service.PackageActivationService;

@RestController
@RequestMapping("/")
public class PackageActivationController {
	
	@Autowired
	private EncryptionValidationService encryptionValidationService;
	
	@Autowired 
	private PackageActivationService packageActivationService;

	@RequestMapping(value = "/v1/init", method = RequestMethod.POST)
    public ResponseEntity<Object> doInit(@RequestHeader("signature-key") String signatureKey, @RequestHeader("secret-key") String secretKey, @RequestBody InitRequest request) {
        if(encryptionValidationService.validationSignature(request.getTerminalId(), secretKey, signatureKey)) {
            return packageActivationService.init(request);
        } else {
            return encryptionValidationService.handleInvalidSignature();
        }
    }
	
	@RequestMapping(value = "/v1/submit-nohp", method = RequestMethod.POST)
    public ResponseEntity<Object> doSubmitNoHp(@RequestHeader("signature-key") String signatureKey, @RequestHeader("secret-key") String secretKey, @RequestBody CustomerProfileRequest request, HttpServletRequest servlet){
        if(encryptionValidationService.validationSignature(request.getTerminalId(), secretKey, signatureKey)) {
            return packageActivationService.submitNoHp(request, servlet);
        } else {
            return encryptionValidationService.handleInvalidSignature();
        }
    }
	
	@RequestMapping(value = "/v1/get-paket", method = RequestMethod.POST)
    public ResponseEntity<Object> doGetPaket(@RequestHeader("signature-key") String signatureKey, @RequestHeader("secret-key") String secretKey, @RequestBody GetPaketRequest request, HttpServletRequest servlet){
        if(encryptionValidationService.validationSignature(request.getTerminalId(), secretKey, signatureKey)) {
            return packageActivationService.getPaket(request, servlet);
        } else {
            return encryptionValidationService.handleInvalidSignature();
        }
    }
	
	@RequestMapping(value = "/v1/payment", method = RequestMethod.POST)
    public ResponseEntity<Object> doPayment(@RequestHeader("signature-key") String signatureKey, @RequestHeader("secret-key") String secretKey, @RequestBody PaymentRequest request, HttpServletRequest servlet){
        if(encryptionValidationService.validationSignature(request.getTerminalId(), secretKey, signatureKey)) {
            return packageActivationService.doPayment(request, servlet);
        } else {
            return encryptionValidationService.handleInvalidSignature();
        }
    }
}
