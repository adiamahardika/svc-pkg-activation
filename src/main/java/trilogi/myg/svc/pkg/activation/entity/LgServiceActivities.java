package trilogi.myg.svc.pkg.activation.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "lg_service_activities")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LgServiceActivities implements Serializable {

	private static final long serialVersionUID = -7619179367913536537L;

	@GenericGenerator(
        name = "lgServiceActivitiesIdSeqGenerator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "lgServiceActivitiesIdSeq"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
        }
    )

    @Id
    @GeneratedValue(generator = "lgServiceActivitiesIdSeqGenerator")
    @JsonIgnore
    private Long id;

    @Column(name = "log_id", updatable = false)
    @JsonIgnore
    private String logId;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "request_from")
    private String requestFrom;

    @Column(name = "request_to")
    private String requestTo;

    @Column(name = "request_data")
    private String requestData;

    @Column(name = "response_data")
    private String responseData;

    @Column(name = "request_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp requestTime;

    @Column(name = "response_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp responseTime;

    @Column(name = "log_by")
    private String logBy;

    @Column(name = "log_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp logDate;

    @Column(name = "service_type")
    private String serviceType;

    @Column(name = "http_status_code")
    private String httpStatusCode;

    @Column(name = "total_time")
    private int totalTime;

    public LgServiceActivities() {
    }

    public LgServiceActivities(String terminalId, String transactionId, String requestFrom, String requestTo, String requestData, String responseData, Timestamp requestTime, Timestamp responseTime, String logBy, String serviceType) {
        this.transactionId = transactionId;
        this.requestFrom = requestFrom;
        this.requestTo = requestTo;
        this.requestData = requestData;
        this.responseData = responseData;
        this.requestTime = requestTime;
        this.responseTime = responseTime;
        this.logBy = logBy;
        this.serviceType = serviceType;
        this.totalTime = (int) (this.getResponseTime().getTime() - this.requestTime.getTime());
        this.logId = terminalId + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public String getRequestTo() {
        return requestTo;
    }

    public void setRequestTo(String requestTo) {
        this.requestTo = requestTo;
    }

    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public Timestamp getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Timestamp requestTime) {
        this.requestTime = requestTime;
    }

    public Timestamp getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Timestamp responseTime) {
        this.responseTime = responseTime;
    }

    public String getLogBy() {
        return logBy;
    }

    public void setLogBy(String logBy) {
        this.logBy = logBy;
    }

    public Timestamp getLogDate() {
        return logDate;
    }

    public void setLogDate(Timestamp logDate) {
        this.logDate = logDate;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}
