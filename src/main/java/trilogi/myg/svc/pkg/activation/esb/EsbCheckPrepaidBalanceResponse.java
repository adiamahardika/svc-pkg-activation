package trilogi.myg.svc.pkg.activation.esb;

import trilogi.myg.svc.pkg.activation.model.NetworkProfile;
import trilogi.myg.svc.pkg.activation.model.Services;

public class EsbCheckPrepaidBalanceResponse extends EsbResponse {
	private Services service;
	private NetworkProfile networkProfile;
	private String amount;
	
	public Services getService() {
        return service;
    }

    public void setService(Services service) {
        this.service = service;
    }
    
    public NetworkProfile getNetworkProfile() {
        return networkProfile;
    }

    public void setNetworkProfile(NetworkProfile networkProfile) {
        this.networkProfile = networkProfile;
    }
    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
