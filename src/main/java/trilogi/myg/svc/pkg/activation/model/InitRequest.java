package trilogi.myg.svc.pkg.activation.model;

public class InitRequest {
    private String terminalId;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
}
