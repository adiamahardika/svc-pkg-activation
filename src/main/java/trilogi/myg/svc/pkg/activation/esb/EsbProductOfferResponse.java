package trilogi.myg.svc.pkg.activation.esb;

import java.util.List;

import trilogi.myg.svc.pkg.activation.model.Offer;

public class EsbProductOfferResponse extends EsbResponse{
	private String confirmationMessage;
	private List<Offer> offer;
	
	public String getConfirmationMessage() {
		return confirmationMessage;
	}
	
	public void setConfirmationMessage(String confirmationMessage) {
		this.confirmationMessage = confirmationMessage;
	}
	
	public List<Offer> getOffer() {
		return offer;
	}
	
	public void setOffer(List<Offer> offer) {
		this.offer = offer;
	}
}
