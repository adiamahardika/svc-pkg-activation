package trilogi.myg.svc.pkg.activation.model;

import java.io.Serializable;

import trilogi.myg.svc.pkg.activation.constant.StandardResponse;
import trilogi.myg.svc.pkg.activation.entity.Terminal;
import trilogi.myg.svc.pkg.activation.entity.TerminalConfig;

public class InitResponse extends StandardResponse implements Serializable {

	private static final long serialVersionUID = 6161243096279492595L;

	private Terminal terminal;
	private TerminalConfig config;
	private String transactionId;

	public Terminal getTerminal() {
		return terminal;
	}

	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	public TerminalConfig getConfig() {
		return config;
	}

	public void setConfig(TerminalConfig config) {
		this.config = config;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
}
