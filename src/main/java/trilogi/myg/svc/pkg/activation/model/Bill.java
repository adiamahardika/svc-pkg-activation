package trilogi.myg.svc.pkg.activation.model;

public class Bill {
	private String serviceId;
	private String balance;
	
	public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    
    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
