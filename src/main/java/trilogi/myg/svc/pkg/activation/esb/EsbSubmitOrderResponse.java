package trilogi.myg.svc.pkg.activation.esb;

public class EsbSubmitOrderResponse extends EsbResponse{
	private String iseligible;
	private String serviceIdA;
	private String notification;
	
	public String getIseligible() {
        return iseligible;
    }

    public void setIseligible(String iseligible) {
        this.iseligible = iseligible;
    }
    
    public String getServiceIdA() {
        return serviceIdA;
    }

    public void setServiceIdA(String serviceIdA) {
        this.serviceIdA = serviceIdA;
    }
    
    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
}
