package trilogi.myg.svc.pkg.activation.esb;

public class EsbResponse {

    private String transactionId;
    private String responseCode;
    private String responseMessage;
    private String errorCode;

    private String statusCode;
    private String httpStatusEsb;
    private Object dataEsb;

    private Long requestTime;
    private Long responseTime;
    private String url;
    private Object dataToEsb;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getHttpStatusEsb() {
        return httpStatusEsb;
    }

    public void setHttpStatusEsb(String httpStatusEsb) {
        this.httpStatusEsb = httpStatusEsb;
    }

    public Object getDataEsb() {
        return dataEsb;
    }

    public void setDataEsb(Object dataEsb) {
        this.dataEsb = dataEsb;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }

    public Long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Long responseTime) {
        this.responseTime = responseTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getDataToEsb() {
        return dataToEsb;
    }

    public void setDataToEsb(Object dataToEsb) {
        this.dataToEsb = dataToEsb;
    }

    @Override
    public String toString() {
        return "EsbResponse{" +
                "transactionId='" + transactionId + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", dataEsb=" + dataEsb +
                ", requestTime=" + requestTime +
                ", responseTime=" + responseTime +
                ", url='" + url + '\'' +
                ", dataToEsb=" + dataToEsb +
                '}';
    }
}
