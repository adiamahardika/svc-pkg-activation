package trilogi.myg.svc.pkg.activation.model;

public class Bonus {
	private String quota;
	private String validity;
    
    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }
    
    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }
}
