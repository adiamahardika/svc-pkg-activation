package trilogi.myg.svc.pkg.activation.bisnis.model;

public class LogTransactionDetail {
    private String datetime;
    private String ipAddress;
    private String hostname;
    private String logId;
    private String mitra;
    private String terminalId;
    private String area;
    private String event;
    private String step;
    private String noHp;
    private String url;
    private String response;
    private String responseStatusCode;
    private String responseTime;
    private String urlEsb;
    private String backendRequestTime;
    private String esbResponse;
    private String esbHttpStatusCode;
    private String esbResponseTime;

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getMitra() {
        return mitra;
    }

    public void setMitra(String mitra) {
        this.mitra = mitra;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponseStatusCode() {
        return responseStatusCode;
    }

    public void setResponseStatusCode(String responseStatusCode) {
        this.responseStatusCode = responseStatusCode;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getUrlEsb() {
        return urlEsb;
    }

    public void setUrlEsb(String urlEsb) {
        this.urlEsb = urlEsb;
    }

    public String getBackendRequestTime() {
        return backendRequestTime;
    }

    public void setBackendRequestTime(String backendRequestTime) {
        this.backendRequestTime = backendRequestTime;
    }

    public String getEsbResponse() {
        return esbResponse;
    }

    public void setEsbResponse(String esbResponse) {
        this.esbResponse = esbResponse;
    }

    public String getEsbHttpStatusCode() {
        return esbHttpStatusCode;
    }

    public void setEsbHttpStatusCode(String esbHttpStatusCode) {
        this.esbHttpStatusCode = esbHttpStatusCode;
    }

    public String getEsbResponseTime() {
        return esbResponseTime;
    }

    public void setEsbResponseTime(String esbResponseTime) {
        this.esbResponseTime = esbResponseTime;
    }

    @Override
    public String toString() {
        return datetime + "|" + ipAddress + "|" + hostname + "|" + logId + "|" + mitra + "|" +
               terminalId + "|" + area + "|" + event + "|" + step + "|" + noHp + "|" + url + "|" +
               response + "|" + responseStatusCode + "|" + responseTime + "|" + urlEsb + "|" +
               backendRequestTime + "|" + esbResponse + "|" + esbHttpStatusCode + "|" + esbResponseTime;
    }
}
