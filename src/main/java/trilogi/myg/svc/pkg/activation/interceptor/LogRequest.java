package trilogi.myg.svc.pkg.activation.interceptor;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.springframework.http.HttpHeaders;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LogRequest {
    private String startRequest;
    private String url;
    private String method;
    private HttpHeaders header;
    private String requestBody;


    public LogRequest(String startRequest, String url, String method, HttpHeaders header, String requestBody) {

        this.startRequest = startRequest;
        this.url = url;
        this.method = method;
        this.header = header;
        this.requestBody = requestBody;
    }

    public String getStartRequest() {
        return startRequest;
    }

    public void setStartRequest(String startRequest) {
        this.startRequest = startRequest;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public HttpHeaders getHeader() {
        return header;
    }

    public void setHeader(HttpHeaders header) {
        this.header = header;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
}
