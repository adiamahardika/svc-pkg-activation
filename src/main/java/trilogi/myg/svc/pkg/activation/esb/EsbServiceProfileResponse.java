package trilogi.myg.svc.pkg.activation.esb;

import trilogi.myg.svc.pkg.activation.model.Services;

public class EsbServiceProfileResponse extends EsbResponse{
	private Services service;
	
	public Services getService() {
        return service;
    }

    public void setService(Services service) {
        this.service = service;
    }
}
