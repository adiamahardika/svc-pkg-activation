package trilogi.myg.svc.pkg.activation.interceptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {

    private final Logger log = LogManager.getLogger("EsbIntegrationLog");
    private final AtomicLong LAST_TIME_MS = new AtomicLong();

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        long start = getUniqueCurrentTime();
        ClientHttpResponse response = clientHttpRequestExecution.execute(httpRequest, bytes);
        long finish = getUniqueCurrentTime();
        logRequest(httpRequest, bytes, start, response, finish);
        return response;
    }

    private void logRequest(HttpRequest request, byte[] body, long start, ClientHttpResponse response, long finish) throws IOException {
        if (log.isDebugEnabled()) {
            String requestTime = new SimpleDateFormat("HH:mm:ss:S").format(new Date(start));
            LogRequest logRequest= new LogRequest(requestTime, request.getURI().toString(), request.getMethod().toString(), request.getHeaders(), new String(body, "UTF-8"));
            ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(logRequest);
            String responseTime = new SimpleDateFormat("HH:mm:ss.S").format(new Date(finish));
            String bodyString = "";
            if (response.getRawStatusCode() == 200) {
            	bodyString = StreamUtils.copyToString(response.getBody(), Charset.defaultCharset());
            } else {
            	bodyString = StreamUtils.copyToString(response.getBody(), Charset.defaultCharset());
            }
            LogResponse res = new LogResponse(responseTime, String.valueOf(response.getRawStatusCode()), response.getStatusText(), response.getHeaders(), bodyString);
            String responseString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(res);
            log.debug("Log Id: {}\nRequest  : {}\nResponse : {}\nResponse Time : {} ms", start, jsonString, responseString, (finish - start));
        }
    }

    private long getUniqueCurrentTime() {
        long now = System.currentTimeMillis();
        while (true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now) now = lastTime + 1;
            if (LAST_TIME_MS.compareAndSet(lastTime, now)) return now;
        }
    }
}
