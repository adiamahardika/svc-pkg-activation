package trilogi.myg.svc.pkg.activation.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "st_terminal_config")
public class TerminalConfig implements Serializable{

    private static final long serialVersionUID = 8888031925444298025L;

	@GenericGenerator(
        name = "stTerminalConfigIdSeqGenerator",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "stTerminalConfigIdSeq"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
        }
    )

    @Id
    @GeneratedValue(generator = "stTerminalConfigIdSeqGenerator")
    @JsonIgnore
    private Long id;

    @Column(name = "terminal_id")
    @JsonIgnore
    private String terminalId;

    @Column(name = "menu_config_id")
    private String menuConfigId;

    @Column(name = "max_failed_ektp")
    private String maxFailedEktp;

    @Column(name = "max_retry_perso")
    private String maxRetryPerso;

    @Column(name = "tid_edc")
    private String tidEdc;

    @Column(name = "mid_edc")
    private String midEdc;

    @Column(name = "sn_edc")
    private String snEdc;

    @Column(name = "buid")
    private String buid;

    @Column(name = "unit_id")
    private String unitId;

    @Column(name = "sam_id")
    private String samId;

    @Column(name = "card_dispenser")
    private String cardDispenser;

    @Column(name = "edc_status")
    private String edcStatus;

    @Column(name = "cash_status")
    private String cashStatus;

    @Column(name = "mac_address_dongle")
    private String macAddressDongle;

    @Column(name = "mac_address_lan")
    private String macAddressLan;

    @Column(name = "vm_id")
    private String vmId;

    @JsonIgnore
    @Column(name = "token")
    private String token;

    @JsonIgnore
    @Column(name = "created_by")
    private String createdBy;

    @JsonIgnore
    @Column(name = "created_date")
    private Timestamp createdDate;

    @JsonIgnore
    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnore
    @Column(name = "updated_date")
    private Timestamp updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMenuConfigId() {
        return menuConfigId;
    }

    public void setMenuConfigId(String menuConfigId) {
        this.menuConfigId = menuConfigId;
    }

    public String getMaxFailedEktp() {
        return maxFailedEktp;
    }

    public void setMaxFailedEktp(String maxFailedEktp) {
        this.maxFailedEktp = maxFailedEktp;
    }

    public String getMaxRetryPerso() {
        return maxRetryPerso;
    }

    public void setMaxRetryPerso(String maxRetryPerso) {
        this.maxRetryPerso = maxRetryPerso;
    }

    public String getTidEdc() {
        return tidEdc;
    }

    public void setTidEdc(String tidEdc) {
        this.tidEdc = tidEdc;
    }

    public String getMidEdc() {
        return midEdc;
    }

    public void setMidEdc(String midEdc) {
        this.midEdc = midEdc;
    }

    public String getSnEdc() {
        return snEdc;
    }

    public void setSnEdc(String snEdc) {
        this.snEdc = snEdc;
    }

    public String getBuid() {
        return buid;
    }

    public void setBuid(String buid) {
        this.buid = buid;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getCardDispenser() {
        return cardDispenser;
    }

    public void setCardDispenser(String cardDispenser) {
        this.cardDispenser = cardDispenser;
    }

    public String getEdcStatus() {
        return edcStatus;
    }

    public void setEdcStatus(String edcStatus) {
        this.edcStatus = edcStatus;
    }

    public String getCashStatus() {
        return cashStatus;
    }

    public void setCashStatus(String cashStatus) {
        this.cashStatus = cashStatus;
    }

    public String getMacAddressDongle() {
        return macAddressDongle;
    }

    public void setMacAddressDongle(String macAddressDongle) {
        this.macAddressDongle = macAddressDongle;
    }

    public String getMacAddressLan() {
        return macAddressLan;
    }

    public void setMacAddressLan(String macAddressLan) {
        this.macAddressLan = macAddressLan;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getVmId() {
        return vmId;
    }

    public void setVmId(String vmId) {
        this.vmId = vmId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getSamId() {
        return samId;
    }

    public void setSamId(String samId) {
        this.samId = samId;
    }
}
