package trilogi.myg.svc.pkg.activation.model;

import java.util.List;

public class Offer {

	private String offerHighlight;
	private String productOfferName;
	private String productLongDescription;
	private String offerId;
	private String businessProductName;
	private String offerPrice;
	private String productLength;
	private String offerPriority;
	private String campaignOffer;
	private String productSubCategory;
	private String productSubCategoryDescription;
	private String termsAndCondition;
	private String originalPrice;
	private String promotionalFlag;
	private String adnMt;
	private List<OffersList> offersList;
	private List<ChargeList> chargeList;
	
	public String getOfferHighlight() {
        return offerHighlight;
    }

    public void setOfferHighlight(String offerHighlight) {
        this.offerHighlight = offerHighlight;
    }
    
    public String getProductOfferName() {
        return productOfferName;
    }

    public void setProductOfferName(String productOfferName) {
        this.productOfferName = productOfferName;
    }
    
    public String getProductLongDescription() {
        return productLongDescription;
    }

    public void setProductLongDescription(String productLongDescription) {
        this.productLongDescription = productLongDescription;
    }
    
    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
    
    public String getBusinessProductName() {
        return businessProductName;
    }

    public void setBusinessProductName(String businessProductName) {
        this.businessProductName = businessProductName;
    }
    
    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }
    
    public String getProductLength() {
        return productLength;
    }

    public void setProductLength(String productLength) {
        this.productLength = productLength;
    }
    
    public String getOfferPriority() {
        return offerPriority;
    }

    public void setOfferPriority(String offerPriority) {
        this.offerPriority = offerPriority;
    }
    
    public String getCampaignOffer() {
        return campaignOffer;
    }

    public void setCampaignOffer(String campaignOffer) {
        this.campaignOffer = campaignOffer;
    }
    
    public String getProductSubCategory() {
        return productSubCategory;
    }

    public void setProductSubCategory(String productSubCategory) {
        this.productSubCategory = productSubCategory;
    }
    
    public String getProductSubCategoryDescription() {
        return productSubCategoryDescription;
    }

    public void setProductSubCategoryDescription(String productSubCategoryDescription) {
        this.productSubCategoryDescription = productSubCategoryDescription;
    }
    
    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }
    
    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }
    
    public String getPromotionalFlag() {
        return promotionalFlag;
    }

    public void setPromotionalFlag(String promotionalFlag) {
        this.promotionalFlag = promotionalFlag;
    }
    
    public String getAdnMt() {
        return adnMt;
    }

    public void setAdnMt(String adnMt) {
        this.adnMt = adnMt;
    }
    
    public List<OffersList> getOffersList() {
        return offersList;
    }

    public void setOffersList(List<OffersList> offersList) {
        this.offersList = offersList;
    }
    
    public List<ChargeList> getChargeList() {
        return chargeList;
    }

    public void setChargeList(List<ChargeList> chargeList) {
        this.chargeList = chargeList;
    }
}
