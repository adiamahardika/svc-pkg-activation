package trilogi.myg.svc.pkg.activation.model;

public class Account {
	private String accountId;
	
	public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
