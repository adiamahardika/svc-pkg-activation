package trilogi.myg.svc.pkg.activation.bisnis.payment;

import com.fasterxml.jackson.annotation.JsonInclude;

import trilogi.myg.svc.pkg.activation.constant.StandardResponse;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PaymentResponse extends StandardResponse{
	
	private static final long serialVersionUID = 6536449038288806954L;
	
	private String iseligible;
	private String serviceIdA;
	private String notification;
	
	public String getIseligible() {
        return iseligible;
    }

    public void setIseligible(String iseligible) {
        this.iseligible = iseligible;
    }
    
    public String getServiceIdA() {
        return serviceIdA;
    }

    public void setServiceIdA(String serviceIdA) {
        this.serviceIdA = serviceIdA;
    }
    
    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
    
}
